server {
    listen 80;
    listen [::]:80 default ipv6only=on;
    server_name localhost;

    root /app/public;

    location / {
        try_files $uri /index.php$is_args$args;         # try to serve file directly, fallback to index.php
    }

    location ~ ^/index\.php(/|$) {
        # fastcgi_pass unix:/var/run/php/php8.2-fpm.sock;         # when using PHP-FPM as a unix socket
        fastcgi_pass 127.0.0.1:9000;         # when PHP-FPM is configured to use TCP
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;

        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
        fastcgi_buffer_size 128k;
        fastcgi_buffers 4 256k;
        fastcgi_busy_buffers_size 256k;

        internal;
    }

    location ~ \.php$ {
        return 404;
    }

    error_log /var/log/nginx/project_error.log;
    access_log /var/log/nginx/project_access.log;
}
