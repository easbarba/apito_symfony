<?php

declare(strict_types=1);

// Ruokin-Symfony is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Ruokin-Symfony is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Ruokin-Symfony. If not, see <https://www.gnu.org/licenses/>.

namespace App\Tests\Integration\Controller;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;

final class HomeControllerTest extends ApiTestCase
{
    public function testIndex(): void
    {
        $response = static::createClient()->request('GET', '/api/v1/');

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains(['data' => 'Ruokin - Evaluate soccer referees performance.']);
    }
}
