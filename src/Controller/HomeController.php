<?php

declare(strict_types=1);

// ruokin-symfony is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ruokin-symfony is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ruokin-symfony. If not, see <https://www.gnu.org/licenses/>.

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/', name: 'home_')]
final class HomeController extends AbstractController
{
    #[Route(path: '/', name: 'app_home', methods: [Request::METHOD_GET])]
    public function index(): JsonResponse
    {
        $result = $this->json(
            data: ['data' => 'Ruokin - Evaluate soccer referees performance.'],
            status: Response::HTTP_OK,
            headers: [
                'Allow' => Request::METHOD_GET,
                'Content-Type' => 'application/json',
            ]
        );
        return $result;
    }
}
